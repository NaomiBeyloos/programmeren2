﻿using System;

namespace Dino
{
    class Program
    {
        static void Main(string[] args)
        {



            //Console.WriteLine(TryOut.ReadFromCSVFile());
            //TryOut.ArrayListSample();
            //TryOut.DictionarySample();
            Console.WriteLine(TryOut.GenericListSimpleExample());
            Console.ReadKey();

        }


        // Dit voorbeeld illustreert het gebruik van de List generieke klasse.
        // Eigenschappen en methoden van List generieke klasse.
        public static string GenericListSimpleExample()
        {
            StringBuilder sb = new StringBuilder();
            List<string> dinosaurs = new List<string>();

            sb.AppendLine(string.Format("\nCapacity: {0}",
            dinosaurs.Capacity));
            // Elementen toevoegen aan het einde van de lijst
            dinosaurs.Add("Tyrannosaurus");
            dinosaurs.Add("Amargasaurus");
            dinosaurs.Add("Mamenchisaurus");
            dinosaurs.Add("Deinonychus");
            dinosaurs.Add("Compsognathus");
            sb.AppendLine();
            foreach (string dinosaur in dinosaurs)
            {
                sb.AppendLine(dinosaur);
            }
            // statistics
            sb.AppendLine(string.Format("\nCapacity: {0}", dinosaurs.Capacity));
            sb.AppendLine(string.Format("Count: {0}", dinosaurs.Count));
            // zit een bepaald element in de lijst?
            sb.AppendLine(string.Format("\nContains(\"Deinonychus\"): {0}",
            dinosaurs.Contains("Deinonychus")));
            // Een rij toevoegen op een bepaalde positie in de lijst
            sb.AppendLine(string.Format("\nInsert(2, \"Compsognathus\")"));
            dinosaurs.Insert(2, "Compsognathus");
            sb.AppendLine();
            foreach (string dinosaur in dinosaurs)
            {
                sb.AppendLine(dinosaur);
            }

            sb.AppendLine(string.Format("\ndinosaurs[3]: {0}",
            dinosaurs[3]));
            // Een rij verwijderen
            sb.AppendLine("\nRemove(\"Compsognathus\")");
            dinosaurs.Remove("Compsognathus");
            sb.AppendLine();
            foreach (string dinosaur in dinosaurs)
            {
                sb.AppendLine(dinosaur);
            }
            // reorganiseert de lijst, lege (gedelete) plaatsen worden verwijderd
            sb.AppendLine("\nStats voor TrimExcess()");
            sb.AppendLine(string.Format("Capacity: {0}", dinosaurs.Capacity));
            sb.AppendLine(string.Format("Count: {0}", dinosaurs.Count));
            dinosaurs.TrimExcess();
            sb.AppendLine("\nStats na TrimExcess()");
            sb.AppendLine(string.Format("Capacity: {0}", dinosaurs.Capacity));
            sb.AppendLine(string.Format("Count: {0}", dinosaurs.Count));
            // ordenen
            sb.AppendLine("\nGeordend op naam");
            dinosaurs.Sort();
            foreach (string dinosaur in dinosaurs)
            {
                sb.AppendLine(dinosaur);
            }
            // voeg een reeks toe, het zijn wel geen dino's, maar
            // dat maakt niet veel uit
            // let erop dat die niet geordend zijn
            String[] names = { "Samuel", "Dakota", "Koani", "Saya", "Vanya",
                    "Yiska", "Yuma", "Jody", "Nikita" };
            dinosaurs.AddRange(names);
            sb.AppendLine("\nReeks namen toegevoegd");
            foreach (string dinosaur in dinosaurs)
            {
                sb.AppendLine(dinosaur);
            }
            // lijst leegmaken
            dinosaurs.Clear();
            sb.AppendLine("\nClear()");
            sb.AppendLine(string.Format("Capacity: {0}", dinosaurs.Capacity));
            sb.AppendLine(string.Format("Count: {0}", dinosaurs.Count));
            return sb.ToString();
        }
    }
}