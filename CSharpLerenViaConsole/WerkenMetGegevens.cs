﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace CSharpLerenViaConsole
{
    class WerkenMetGegevens
    {
        public static void CharLerenGebruiken()
        {
            char character = 'B';

            Console.WriteLine("karakter in hoofdletter {0}", char.ToUpper(character));

            Console.WriteLine("is karakter gelijk aan b? {0}", character.Equals('b'));

            Console.WriteLine("Raad het juiste karakter om verder te gaan");

            // sentinel

            bool guessed = false;
            while (!guessed)
            {
                ConsoleKeyInfo guessCharacter = Console.ReadKey();

                if (guessCharacter.KeyChar.Equals('B'))

                {

                    guessed = true;

                }

            }

            Console.WriteLine("Goed geraden!");

        }

        public void ShowAllAsciiValues()
        {

            for (int i = 0; i <= 255; i++)
            {

                char character = (char)i;
                string symbol;

                if (char.IsControl(character))
                {
                    symbol = "Control character";
                }

                else
                {
                    symbol = character.ToString();

                }


                Console.WriteLine("ASCI \t{0} \tSymbool \t{1}",
                    i, symbol);
            }

        }

        static public void showCultureInfo()
        {
            CultureInfo cultureInfo = new CultureInfo("nl-BE");
            Console.Write("{0,-7}", cultureInfo.Name);
            Console.Write(" {0,-3}", cultureInfo.TwoLetterISOLanguageName);
            Console.Write(" {0,-3}", cultureInfo.ThreeLetterISOLanguageName);
            Console.Write(" {0,-3}", cultureInfo.ThreeLetterWindowsLanguageName);
            Console.Write(" {0,-40}", cultureInfo.DisplayName);
            Console.WriteLine(" {0,-40}", cultureInfo.EnglishName);
        }

        static public void stringConcatenationVersusStringBuilder()
        {
            string voornaam = "Mohamed";
            string familienaam = "El Farisi";
            string beroep = "Docent";
            Console.WriteLine("Mijn collega heet " + voornaam + familienaam + " en zijn beroep is " + beroep);
            Console.ReadKey();
            // boerse manier

            StringBuilder sb = new StringBuilder();
            sb.Append("Mijn collega heet ");
            sb.Append(voornaam);
            sb.Append(" ");
            sb.Append(familienaam);
            sb.Append(" en zijn beroep is ");
            sb.Append(beroep);
            // elegante manier

            sb.AppendFormat("Mijn collega heet {0} {1} en zijn beroep is {2}", voornaam, familienaam, beroep);
            // beste manier

            Console.WriteLine(sb.ToString());
            sb.Clear();

            voornaam = "Jan";
            familienaam = "Jansens";
            beroep = "professioneel zeveraar";

            sb.AppendFormat("Mijn collega heet {0} {1} en zijn beroep is {2}", voornaam, familienaam, beroep);
            Console.WriteLine(sb.ToString());
            sb.Clear();

        }



        // Dit is een gegevenstype en geen variabele!
        // Je kan geen enum in een methode aanmaken,
        // wel op klasse niveau of op namespace niveau.
        public enum DagenVanDeWeek
        {
            Ma, Di, Wo,
            Do, Vr, Za, Zo
        };

        class EnumSamples
        {
            public static string ASimpleEnumSample(DagenVanDeWeek d)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Een eenvoudig voorbeeld:");
                int maandag = (int)DagenVanDeWeek.Ma;
                int dinsdag = (int)DagenVanDeWeek.Di;
                sb.AppendFormat("De waarde van Di is {0}, de waarde van Ma is {1}", dinsdag, maandag);
                if (d == DagenVanDeWeek.Ma)
                {
                    sb.AppendLine("Je hebt maandag als parameter meegegeven.");
                }
                return sb.ToString();
            }




        }

        public void WerkenMetStruct()
        {
            Meetkunde.Point mijnPunt = new Meetkunde.Point(4, 5);
            for (int i = 1; i < mijnPunt.y; i++)
            {
                Console.WriteLine();
            }
            for (int i = 1; i < mijnPunt.y; i++)
            {
                Console.Write(' ');
            }

            Console.Write('*');
            Console.ReadKey();
        }

        public static void TryOutListArray()
        {
            ArrayList namen = new ArrayList();

            namen.Add("Mohamed");
            namen.Add("Betty");
            namen.Add("Jan");

            foreach (string naam in namen)
            {
                Console.WriteLine(naam);

            }
            Console.ReadKey();
        }

        public static void WerkenMetDictionary()
        {
            Dictionary<string, int> leeftijd = new Dictionary<string, int>();
            leeftijd.Add("Mohamed", 20);            // string is de key, int is de value, mohamed is key, 20 is value
            leeftijd.Add("Betty", 28);
            leeftijd.Add("Jan", 30);

            foreach (KeyValuePair<string, int> persoon in leeftijd)
            {
                Console.WriteLine("De leeftijd van {0} is {1}.", persoon.Key, persoon.Value);
            }
        }
    }
}
