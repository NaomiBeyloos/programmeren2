﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Blog
{
    class Persoon
    {
        private string voornaam;
        private string familienaam;
        private string straat;
        private string postcode;
        private string stad;


        public string Voornaam
        {
            get { return voornaam; }
            set { voornaam = value; }
        }

        public string Familienaam
        {
            get { return familienaam; }
            set { familienaam = value; }
        }
        public string Straat
        {
            get { return straat; }
            set { straat = value; }
        }
        public string Postcode
        {
            get { return postcode; }
            set { postcode = value; }
        }
        public string Stad
        {
            get { return stad; }
            set { stad = value; }
        }
        public string ShowInfo()
        {
            return $"voornaam: {voornaam}\nfamilienaam: {familienaam}";

        }

        public string SerializeObjectToCsv(List<Persoon> list, string separator)
        {
            string fileName = @"C:\Crescendo\Programmeren2\CSharpLerenViaConsole/Data/Personen.csv";
            string message;
            try
            {
                TextWriter writer = new StreamWriter(fileName);
                foreach (Persoon item in list)
                {
                    // One of the most versatile and useful additions to the C# language in version 6
                    // is the null conditional operator ?.           
                    writer.WriteLine("{0}{5}{1}{5}{2}{5}{3}{5}{4}",
                        item?.Voornaam,
                        item?.Familienaam,
                        item?.Straat,
                        item?.Postcode,
                        item?.Stad,
                        separator);
                }
                writer.Close();
                message = $"Het bestand met de naam {fileName} is gemaakt!";
            }
            catch (Exception e)
            {
                // Melding aan de gebruiker dat iets verkeerd gelopen is.
                // We gebruiken hier de nieuwe mogelijkheid van C# 6: string interpolatie
                message = $"Kan het bestand met de naam {fileName} niet maken.\nFoutmelding {e.Message}.";
            }
            return message;
        }

        public string ReadFromCSVFile()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = "Data\\Personen.csv";
            // lees het bestand in vanop de harde schijf
            bestand.Lees();
            return bestand.Text;
        }

        public string[] GetArray()
        {
            string text = ReadFromCSVFile();
            string[] personen = text.Split('\n');
            return personen;
        }

        public List<Persoon> GetList(string[] personen)
        {
            List<Persoon> personenLijst = new List<Persoon>();
            foreach (string item in personen)
            {
                string[] persoonsgegevens = item.Split(';');
                Persoon persoon = new Persoon();
                persoon.Voornaam = item[0].ToString();
                persoon.Familienaam = item[1].ToString();
                persoon.Straat = item[2].ToString();
                persoon.Postcode = item[3].ToString();
                persoon.Stad = item[4].ToString();
                personenLijst.Add(persoon);
            }
            return personenLijst;
        }
    }
}