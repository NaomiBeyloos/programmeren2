﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpLerenViaConsole.Meetkunde
{
    public struct Point    // structure is Point
    {
        public readonly  int x;  //alleen zichtbaar tijdens de struct
        public readonly int y;

        public Point(int x, int y)// public om het zichtbaar te maken
        {
            this.x = x;         //this staat voor de private
            this.y = y;
        }
    }

}
