﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Helpers
{
    class Tekstbestand
    {
        /*declaratie: naam van het veld in camelcase notatie
         gegevenstype van het veld is string
         het bereik is de klasse, d.w.z. het veld
         is buiten de klasse niet zichtbaar (alleen zichtbaar in de klasse zelf)
         kan dus alleen binnen de klasse gebruikt worden
         met public kan je het wel oproepen buiten de klasse en ook aanpassen*/
        private string text;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        private string melding;
        public string Melding
        {
            get { return melding; }
            set { melding = value; }
        }

        private string fileName;
        public string FileName
        {
            get { return fileName; }
            set {
                if (value.Length== 0)
                {
                    Melding = "Je moet een bestandsnaam opgeven!";
                    fileName = "";
                }
                else
                {
                    fileName = value;
                    Melding = "Bestandsnaam is oké!";
                }
            }
            
        }

        // incapsulatie
        public string FullName
        {
            get { return $"c:\\documents\\{fileName}"; }
        }

        public bool Lees() // Lees is camelcase notatie
        {
            bool result = false; //wachter: we gaan er van uit  dat het niet lukt
            try
            {
                using (StreamReader sr = new StreamReader(this.FileName))   // new StreamReader is een nieuwe instantie die we gemaakt hebben
                {
                    this.Text = sr.ReadToEnd();
                    this.Melding = $"Het bestand met de naam {this.FileName} is ingelezen.";  //$ --> string interpolatie: tekst mengen met variabelen
                    result = true;
                }
            }

            catch (Exception e)

            {
                this.Melding = $"Het bestand met de naam {this.FileName} is niet ingelezen.\nFout: {e.Message}";
            }
            return result;
        }
    }
}
