﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wiskunde.Meetkunde
{
    class Vormen
    {
        // private field
        // geen toegang van buiten de klasse
        private ConsoleColor kleur;
        // public property
        // om toegang te verlenen aan het private field
        // is de waarde die men wil toekennen aan het private field
        // wel degelijk een blauwe kleur?
        public ConsoleColor Kleur
        {
            get { return kleur; }
            set
            {
                // check als opgegeven kleur een blauwwaarde is
                if (value == ConsoleColor.Blue || value == ConsoleColor.DarkBlue)
                {
                    kleur = value;
                    Console.ForegroundColor = kleur;
                }
                else
                {
                    Console.WriteLine("Kleur moet blauw zijn!");
                }
            }

        }

        public static string Lijn(int lengte)
        {
            return new string('-', lengte);
        }

        public static string Lijn(int lengte, char teken)
        {
            return new string(teken, lengte);
        }

        public static string Lijn(int lengte, ConsoleColor kleur)
        {
            Console.ForegroundColor = kleur;
            return new string('-', lengte);

        }

        public static string Lijn(int lengte, char teken, ConsoleColor kleur)
        {
            Console.ForegroundColor = kleur;
            return new string(teken, lengte);
        }


    }
}

