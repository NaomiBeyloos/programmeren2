﻿using System;
using Blog;
using Fruit;
using System.Collections.Generic;

namespace CSharpLerenViaConsole
{
    class Program
    {
        static void GeefPersoongegevensIn()
        {
            string voornaam;
            string familienaam;
            string straat;
            string postcode;
            string stad;
            List<Persoon> persoonLijst = new List<Persoon>();
            do
            {
                Console.Clear();
                Console.Write("Typ een voornaam in: ");
                voornaam = Console.ReadLine();
                if (voornaam != "Q")
                {
                    Console.Write("Typ een familienaam in: ");
                    familienaam = Console.ReadLine();
                    if (familienaam != "Q")
                    {
                        Console.Write("Typ een straat in: ");
                        straat = Console.ReadLine();
                        if (straat != "Q")
                        {
                            Console.Write("Typ een postcode in: ");
                            postcode = Console.ReadLine();
                            if (postcode != "Q")
                            {
                                Console.Write("Typ een stad in: ");
                                stad = Console.ReadLine();
                                // maakt een instantie/exemplaar van de klasse persoon
                                Persoon persoonsInstantie = new Persoon();
                                // We gebruiken de Voornaam property van de instantie
                                // persoonsInstantie om de waarde van voornaam toe te kennen
                                // aan het verld voornaam van de persoonsInstantie.
                                persoonsInstantie.Voornaam = voornaam;
                                persoonsInstantie.Familienaam = familienaam;
                                persoonsInstantie.Straat = straat;
                                persoonsInstantie.Postcode = postcode;
                                persoonsInstantie.Stad = stad;
                                // we voegen de nieuw gemaakte instantie toe
                                // aan de lijst met persoonsinstanties
                                persoonLijst.Add(persoonsInstantie);
                                Console.WriteLine("Je hebt de volgende persoonsgegevens ingetypt:");
                                Console.WriteLine($"Voornaam: {persoonsInstantie.Voornaam}");
                                Console.WriteLine($"Familienaam: {persoonsInstantie.Familienaam}");
                                Console.ReadKey();
                            }
                        }
                    }
                }
            }
            while (voornaam != "Q");
            // schrijf de lijst met persoonsinstanties naar de harde schijf
            Persoon persoonInstantie = new Persoon();
            persoonInstantie.SerializeObjectToCsv(persoonLijst, ";");


        }

        static void GeefPostcodesIn()
        {
            string postcode;
            string stad;
            string provincie;
            string localite;
            string province;
            List<Postcode> postcodeList = new List<Postcode>();
            do
            {
                Console.Clear();
                Console.Write("Typ een postcode in: ");
                postcode = Console.ReadLine();
                if (postcode == "Q")
                {
                    break;
                }
                Console.Write("Typ de naam van de stad in: ");
                stad = Console.ReadLine();
                if (stad == "Q")
                {
                    break;
                }
                Console.Write("Typ de naam van de provincie in: ");
                provincie = Console.ReadLine();
                if (provincie == "Q")
                {
                    break;
                }
                Console.Write("Typ de naam in het frans van de stad in: ");
                localite = Console.ReadLine();
                if (localite == "Q")
                {
                    break;
                }
                Console.Write("Typ de naam in het frans van de privincie in: ");
                province = Console.ReadLine();
                // ik wil de ingetypte gegevens van de postcode opslaan
                // waarin? Nu staan ze in string variabelen
                // telkens als er een nieuwe postcode wordt ingetypt
                // worden dezelfde string variabelen gebruikt
                // dus de vorige postcode wordt overschreven
                // wat komt ons redden?
                // de Postcode klasse. De klasse bevat een beschrijving van de postcode, dwz maakt
                // een veld en properties voor elk kenmerk
                // van de postcode: Plaats, Stad, Pronvincie, Localite, Ville en een Province
                // ik moet nu eerst een object, een instantie maken van de klasse:


                Postcode objectPostcode = new Postcode();

                // ik wil de waarden die in de string variabelen staan in de velden
                // van de objectPostcode instantie kopiëren
                // daarvoor gebruik ik de setters van de properties:

                objectPostcode.Code = postcode;
                objectPostcode.Plaats = stad;
                objectPostcode.Provincie = provincie;
                objectPostcode.Localite = localite;
                objectPostcode.Province = province;

                // voor elke postcode maak ik een nieuw object, dwz dat
                // de gegevens van de vorige postcode niet worden overgeschreven
                // elk ingegeven postcode heeft zijn eigen object

                // de instantie objectPostcode aan generieke lijst toevoegen
                postcodeList.Add(objectPostcode);
            }

            while (postcode != "Q");
            Console.WriteLine("De lijst van de ingevoerde postcodes:");
            

            foreach (Postcode postcodeItem in postcodeList)
            {
                Console.WriteLine(" {0} {1} {2} {3} {4}, ",
                    postcodeItem.Code, postcodeItem.Plaats, postcodeItem.Provincie, postcodeItem.Localite, postcodeItem.Province);
            }

            Console.WriteLine("Klik op een toets om het programma de beëindigen.");
            Console.ReadKey();

            // beschik over een instantie van Postcode klasse?
            // nee, ik moet dus eerst een instantie van de Postcode klasse maken
            Postcode mijnPostcode = new Postcode();
            string message = mijnPostcode.SerializeObjectToCsv(postcodeList, "$");
            Console.WriteLine(message);
            Console.ReadKey();


        }

        static void Main(string[] args)
        {
            // Console.WriteLine("Hello World!");
            // CSharpLerenViaConsole.WerkenMetGegevens.CharLerenGebruiken();

            // WerkenMetGegevens werkenMetGegevens = new WerkenMetGegevens();
            // werkenMetGegevens.ShowAllAsciiValues();
            // ShowAscii is niet static
            // eerst instantie maken


            // WerkenMetGegevens.showCultureInfo();
            // WerkenMetGegevens.stringConcatenationVersusStringBuilder();
            // Console.ReadKey();

            // WerkenMetGegevens werkenmetGegevens = new WerkenMetGegevens();
            // werkenmetGegevens.WerkenMetStruct();
            // WerkenMetGegevens.TryOutListArray();

            //Persoon afzender = new Persoon(); //instantie
            //afzender.voornaam = "Mohammed";
            //afzender.familienaam = "El Fahrisi";
            //afzender.Leeftijd = 22;
            //Persoon ontvanger = new Persoon();
            //ontvanger.voornaam = "Naomi";
            //ontvanger.familienaam = "Beyloos";
            //ontvanger.Leeftijd = 21;
            //Persoon ontvanger2 = new Persoon();
            //ontvanger2.voornaam = "Josette";
            //ontvanger2.familienaam = "Jansens";
            //ontvanger2.Leeftijd = 233;
            //string afzendertekst = afzender.ShowInfo();
            //Console.WriteLine(afzender.ShowInfo());
            //string ontvangertekst = afzender.ShowInfo();
            //Console.WriteLine(ontvanger.ShowInfo());
            //string ontvanger2tekst = afzender.ShowInfo();
            //Console.WriteLine(ontvanger2.ShowInfo());
            //Console.ReadKey();




            //Product mango = new Product();
            //mango.soort = "Mango";
            //mango.kleur = "Oranje";
            //mango.lengte = 9;
            //mango.rijpheid = "Rijp";
            //Product banaan = new Product();
            //banaan.soort = "Banaan";
            //banaan.kleur = "Groen";
            //banaan.lengte = 10;
            //banaan.rijpheid = "Niet rijp";
            //Product kers = new Product();
            //kers.soort = "Kers";
            //kers.kleur = "Donker rood";
            //kers.lengte = 3;
            //kers.rijpheid = "Rijp";
            //Product appel = new Product();
            //appel.soort = "Appel";
            //appel.kleur = "Groen";
            //appel.lengte = 6;
            //appel.rijpheid = "Rijp";

            //string mangotekst = mango.ShowInfo1();
            //Console.WriteLine(mango.ShowInfo1());
            //string banaantekst = banaan.ShowInfo1();
            //Console.WriteLine(banaan.ShowInfo1());
            //string kerstekst = kers.ShowInfo1();
            //Console.WriteLine(kers.ShowInfo1());
            //string appeltekst = appel.ShowInfo1();
            //Console.WriteLine(appel.ShowInfo1());

            // mijnTekstbestand = gegevenstype
            //Helpers.Tekstbestand mijnTekstbestand = new Helpers.Tekstbestand();
            // de setter van de propperty om een waarde aan het private field
            // toe te kennen
            //mijnTekstbestand.FileName = "data\\Postcode.csv";
            //Console.WriteLine("Melding: {0}", mijnTekstbestand.Melding);

            // de getter van de propperty om de waarde uit het private field
            //op te halen
            //Console.WriteLine("Het bestand heet {0}", mijnTekstbestand.FileName);
            // good practice: manipulation data inside class (encapsulation)
            //Console.WriteLine("De volledige bestandsnaam is {0}", mijnTekstbestand.FullName);
            // bad practice: gegevensmanipulatie buiten de klasse in de calling program
            //Console.WriteLine("De volledige bestandsnaam is c:\\data\\{0}", mijnTekstbestand.FileName);

            // Tekst inlezen
            //if (mijnTekstbestand.Lees())
            // {
            // Toon de ingelezen tekst
            //   Console.WriteLine($"De tekst in een bestand is \n{mijnTekstbestand.Text}"); // \n --> new line
            // }
            //else

            // {
            //    Console.WriteLine($"Het bestand heet {mijnTekstbestand.Melding}");
            // }
            //  Console.WriteLine("Typ een postcode in: ");
            // Console.ReadLine();
            // zoek de postcode
            //Postcode postcode = new Postcode();
            //Console.WriteLine(postcode.ReadPostcodesFromCSVFile());
            //postcode.GetPostcodeList();

            GeefPersoongegevensIn();

            Console.ReadKey();

        }

        
    }
            
    }

