﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Fruit
{
    class Product
    {
        public string soort;
        public string kleur;
        public int lengte;
        public string rijpheid;

        public string ShowInfo1()
        {
            return $"Soort: {soort}\nKleur: {kleur}\nLengte: {lengte}cm\nRijpheid: {rijpheid}";
        }
    }
}