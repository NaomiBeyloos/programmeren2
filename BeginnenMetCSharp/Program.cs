﻿using System;
using Wiskunde.Meetkunde;

namespace BeginnenMetCSharp
{
    class Program
    {
        
        static void Main(string[] args)
        {
            string familienaam = "Jan Janssen";
            Console.WriteLine("Hello World!");
            Console.WriteLine(familienaam);
            Console.WriteLine(Vormen.Lijn(20));
            Console.WriteLine(Vormen.Lijn(50, '*'));
            Console.WriteLine(Vormen.Rechthoek(5));
            Console.WriteLine(Vormen.Rechthoek(2, '|'));
            // Console.WriteLine(Vormen.Rechthoek(2, '|', 2, '-'));
            Console.WriteLine(Vormen.Driehoek(5));
            Console.ReadKey();

            
        }
    }
}
