﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wiskunde.Meetkunde
{
    class Vormen
    {
        private ConsoleColor kleur;
        public ConsoleColor Kleur
        {
            get { return kleur; }
            set
            {
                kleur = value;      // value is impliciete parameter van setter
                Console.ForegroundColor = kleur;
            }
        }

        public static string Lijn(int lengte) // static = methode kunnen oproepen zonder een instantie van deze klasse te moeten
                                              // creeëren
        {
            return new string('-', lengte);
        }

        // public  string Lijn(int lengte) 
        //   {
        //      return new string('-', lengte);
        //   }

        // Dan moet je in Program.cs 
        // Vormen vormen = new Vormen();
        // Console.WriteLine(vormen.Lijn(20));
        // plaatsen

        public static string Lijn(int lengte, char teken) // public = toegankelijk buiten de klasse
        {
            return new string(teken, lengte);
        }


      




        public static string Rechthoek(int lengte)
        {
            ///return new string('-', lengte);
            ///return new string('|', lengte);
            ///return new string('-', lengte);
            ///return new string('|', lengte);

            return new string('-', lengte);


        }

        public static string Rechthoek(int lengte, char teken)
        {
            return new string(teken, lengte);
        }









        public static string Driehoek(int lengte)
        {
            return new string('-', lengte);
        }

        public static string Driehoek(int lengte, char teken)
        {
            return new string(teken, lengte);
        }
    }
}
